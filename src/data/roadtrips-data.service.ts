import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { Roadtrip, RoadtripItem } from '../models/index';


@Injectable()
export abstract class RoadtripsDataService {

  abstract getRoadtrips(): Observable<Roadtrip[]>;
  abstract getRoadtrip(id: string): Observable<Roadtrip>;
  abstract getRoadtripBySlug(slug: string): Observable<Roadtrip>;
  abstract getRoadtripItems(roadtripId: string): Observable<RoadtripItem[]>;
  abstract getRoadtripItem(id: string): Observable<RoadtripItem>;

  abstract addRoadtrip(entity: Roadtrip): Observable<Roadtrip>;
  abstract addRoadtripItem(entity: RoadtripItem): Observable<RoadtripItem>;

  abstract updateRoadtrip(entity: Roadtrip): Observable<Roadtrip>;
  abstract updateRoadtripItem(entity: RoadtripItem): Observable<RoadtripItem>;

  abstract deleteRoadtrip(id: string): Observable<Roadtrip>;
  abstract deleteRoadtripItem(id: string): Observable<RoadtripItem>;

  abstract addRoadtripItemToRoadtrip(roadtripId: string, roadtripItemId: string): Observable<Roadtrip>;
  abstract removeRoadtripItemFromRoadtrip(id: string): Observable<Roadtrip>;
}
