import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';

import { Roadtrip, RoadtripItem } from '../models/index';


export interface RoadtripsDataObject {
  roadtrips: Roadtrip[];
  roadtripsItems: RoadtripItem[];
}


@Injectable()
export class RoadtripsDataObjectService {

  constructor(private data: RoadtripsDataObject) {};

  loadDataObject(data: RoadtripsDataObject) {
    this.data = data;
  }

  getData() {
    return this.data;
  }

  getRoadtrips(): Observable<Roadtrip[]> {
    return Observable.of(this.data.roadtrips);
  };

  getRoadtrip(id: string): Observable<Roadtrip> {
    const result = this.data.roadtrips.find(el => el.id === id);
    return Observable.of(result);
  };

  getRoadtripBySlug(slug: string): Observable<Roadtrip> {
    const result = this.data.roadtrips.find(el => el.slug === slug);
    return Observable.of(result);
  };

  getRoadtripItems(roadtripId: string): Observable<RoadtripItem[]> {
    return this.getRoadtrip(roadtripId)
      .map(roadtrip => {
        return roadtrip.items.map(item => {
          return this.data.roadtripsItems.find(el => el.id === item);
        });
      });
  }

  getRoadtripItem(id: string): Observable<RoadtripItem> {
    const result = this.data.roadtripsItems.find(el => el.id === id);
    return Observable.of(result);
  };


  addRoadtrip(entity: Roadtrip): Observable<Roadtrip> {
    const id = Date.now().toString();
    entity = Object.assign({ id: id, title: id, items: [] }, entity);
    this.data.roadtrips.push(entity);
    return Observable.of(entity);
  };

  addRoadtripItem(entity: RoadtripItem): Observable<RoadtripItem> {
    const id = Date.now().toString();
    entity = Object.assign({ id: id, title: id }, entity);
    this.data.roadtripsItems.push(entity);
    return Observable.of(entity);
  };


  updateRoadtrip(entity: Roadtrip): Observable<Roadtrip> {
    const idx = this.data.roadtrips.findIndex(el => el.id === entity.id);
    if (idx > -1) {
      this.data.roadtrips[idx] = Object.assign({ items: this.data.roadtrips[idx].items }, entity);
    } else {
      // exception?
    }
    return Observable.of(entity);
  };

  updateRoadtripItem(entity: RoadtripItem): Observable<RoadtripItem> {
    const idx = this.data.roadtripsItems.findIndex(el => el.id === entity.id);
    if (idx > -1) {
      this.data.roadtripsItems[idx] = Object.assign({}, entity);
    } else {
      // exception?
    }
    return Observable.of(entity);
  };

  deleteRoadtrip(id: string): Observable<Roadtrip> {
    return Observable.of(null);
  };

  deleteRoadtripItem(id: string): Observable<RoadtripItem> {
    return Observable.of(null);
  };


  addRoadtripItemToRoadtrip(roadtripId: string, roadtripItemId: string): Observable<Roadtrip> {
    return this.getRoadtrip(roadtripId)
      .map(entity => {
        entity.items.push(roadtripItemId);
        return entity;
      })
  };

  removeRoadtripItemFromRoadtrip(id: string): Observable<Roadtrip> {
    return Observable.of(null);
  };
}
