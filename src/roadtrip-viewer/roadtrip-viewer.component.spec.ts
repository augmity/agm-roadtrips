import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadtripViewerComponent } from './roadtrip-viewer.component';

describe('RoadtripViewerComponent', () => {
  let component: RoadtripViewerComponent;
  let fixture: ComponentFixture<RoadtripViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadtripViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadtripViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
