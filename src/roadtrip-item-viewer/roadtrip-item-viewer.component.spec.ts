import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadtripItemViewerComponent } from './roadtrip-item-viewer.component';

describe('RoadtripItemViewerComponent', () => {
  let component: RoadtripItemViewerComponent;
  let fixture: ComponentFixture<RoadtripItemViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadtripItemViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadtripItemViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
