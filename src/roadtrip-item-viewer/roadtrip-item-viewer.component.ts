import { Component, OnInit, Input } from '@angular/core';

import { RoadtripItem } from '../models/index';


@Component({
  selector: 'roadtrip-item-viewer',
  templateUrl: './roadtrip-item-viewer.component.html',
  styleUrls: ['./roadtrip-item-viewer.component.scss']
})
export class RoadtripItemViewerComponent {

  @Input()
    get model(): RoadtripItem {
      return this._model;
    }
    set model(value: RoadtripItem) {
      this._model = value;
    }
  private _model: RoadtripItem;
}
