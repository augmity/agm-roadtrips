import { Directive, Input } from '@angular/core';

import { RoadtripsDataService } from '../data/index';
import { RoadtripItemViewerComponent } from './roadtrip-item-viewer.component';


@Directive({
  selector: '[roadtripItemId]'
})
export class RoadtripItemIdDirective {

  @Input()
    get roadtripItemId(): string {
      return this._roadtripItemId;
    }
    set roadtripItemId(value: string) {
      this._roadtripItemId = value;

      if (this.roadtripItemViewer) {
        this.roadtripsDataService.getRoadtripItem(this.roadtripItemId)
          .subscribe(data => {
            this.roadtripItemViewer.model = data;
          });
      } else {
        throw { message: 'Directive can be used only on a RoadtripItemViewerComponent.' };
      }
    }
  private _roadtripItemId: string;

  constructor(private roadtripItemViewer: RoadtripItemViewerComponent, private roadtripsDataService: RoadtripsDataService) {}
}
