import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { RoadtripEditorComponent } from './roadtrip-editor/roadtrip-editor.component';
import { RoadtripViewerComponent } from './roadtrip-viewer/roadtrip-viewer.component';
import { RoadtripItemViewerComponent } from './roadtrip-item-viewer/roadtrip-item-viewer.component';
import { RoadtripItemEditorComponent } from './roadtrip-item-editor/roadtrip-item-editor.component';
import { RoadtripItemIdDirective } from './roadtrip-item-viewer/roadtrip-item-id.directive';


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  declarations: [
    RoadtripEditorComponent,
    RoadtripViewerComponent,
    RoadtripItemViewerComponent,
    RoadtripItemEditorComponent,
    RoadtripItemIdDirective
  ],
  exports: [
    RoadtripEditorComponent,
    RoadtripViewerComponent,
    RoadtripItemViewerComponent,
    RoadtripItemEditorComponent,
    RoadtripItemIdDirective
  ]
})
export class RoadtripsModule { }
