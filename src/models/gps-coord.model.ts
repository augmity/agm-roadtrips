export interface GpsCoord {
  lat: string;
  long: string;
}
