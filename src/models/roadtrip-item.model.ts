import { UID } from './uid';
import { GpsCoord } from './gps-coord.model';
import { RoadtripItemType } from './roadtrip-item-type.enum';

export interface RoadtripItem {
  id?: UID;
  title?: string;
  description?: string;
  date?: Date;
  imageUri?: string;
  thumbnailUri?: string;
  author?: string;
  gpsCoord?: GpsCoord;
  type?: RoadtripItemType;
}
