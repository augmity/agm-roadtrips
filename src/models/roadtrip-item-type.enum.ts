export enum RoadtripItemType {
  Text = 1,
  Markdown = 2,
  Image = 3,
  Video = 4,
  Map = 5
}

export const RoadtripItemTypeDisplayNames = [
  { id: RoadtripItemType.Text, name: 'Text' },
  { id: RoadtripItemType.Markdown, name: 'Markdown' },
  { id: RoadtripItemType.Image, name: 'Image' },
  { id: RoadtripItemType.Video, name: 'Video' },
  { id: RoadtripItemType.Map, name: 'Map' },
];

