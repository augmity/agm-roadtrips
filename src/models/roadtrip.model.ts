import { UID } from './uid';
import { RoadtripItem } from './roadtrip-item.model';

export interface Roadtrip {
  id?: UID;
  title?: string;
  subtitle?: string;
  description?: string;
  slug?: string;
  date?: Date;
  imageUri?: string;
  author?: string;
  items?: UID[];
}
