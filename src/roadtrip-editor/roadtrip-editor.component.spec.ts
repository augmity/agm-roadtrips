import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadtripEditorComponent } from './roadtrip-editor.component';

describe('RoadtripEditorComponent', () => {
  let component: RoadtripEditorComponent;
  let fixture: ComponentFixture<RoadtripEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadtripEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadtripEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
