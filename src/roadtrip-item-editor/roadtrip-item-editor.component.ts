import { Component, Output, EventEmitter, OnInit, Optional, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor, FormBuilder, FormGroup, Validators } from '@angular/forms';

import { RoadtripItem } from '../models/index';

const RTI_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => RoadtripItemEditorComponent),
  multi: true
};


@Component({
  selector: 'roadtrip-item-editor',
  templateUrl: './roadtrip-item-editor.component.html',
  styleUrls: ['./roadtrip-item-editor.component.scss'],
  providers: [RTI_CONTROL_VALUE_ACCESSOR],
})
export class RoadtripItemEditorComponent implements OnInit {

  @Output() valid = new EventEmitter<boolean>();
  model: RoadtripItem;
  form: FormGroup;

  // Placeholders for the callbacks
  private onTouchedCallback:(_:any) => void;
  private onChangeCallback:(_:any) => void;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.form = this.fb.group({
      'title': [],
      'description': [],
      'date': [],
      'imageUri': []
    });

    this.form.valueChanges
      .subscribe(value => {
        this.onChangeCallback(value);
      });
    this.form.statusChanges
      .subscribe(status => {
        this.valid.emit((status === 'VALID'));
      });
  }

  //   ControlValueAccessor interface implementation
  // -------------------------------------------------

  writeValue(value: any) {
    if (value) {
      this.form.patchValue(value);
    }
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
