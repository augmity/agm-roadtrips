import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadtripItemEditorComponent } from './roadtrip-item-editor.component';

describe('RoadtripItemEditorComponent', () => {
  let component: RoadtripItemEditorComponent;
  let fixture: ComponentFixture<RoadtripItemEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadtripItemEditorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadtripItemEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
